<div id="carouselRoomDeluxeQueen" class="carousel slide carousel-fade" data-interval="false" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="0" class="active"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="1"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="2"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="3"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="4"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="5"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="6"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="7"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="8"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="9"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="10"></li>
    <li data-target="#carouselRoomDeluxeQueen" data-slide-to="11"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/heros/super-deluxe-queen-9.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-3.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-4.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-6.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-7.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-8.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-5.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-10.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-11.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-12.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/super-deluxe-queen-2.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselRoomDeluxeQueen" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselRoomDeluxeQueen" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
