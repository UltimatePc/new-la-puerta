<div id="carouselIntroIndex" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="zoom">
      <div class="carousel-item active">
        <img data-src="/imgs/heros/la-puerta-del-sol-cover.jpg" class="lazy d-block w-100" style="max-height: 800px" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="carousel-item">
        <img data-src="/imgs/heros/la-puerta-del-sol-cover-2.jpg" class="lazy d-block w-100" style="max-height: 800px" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="carousel-item">
        <img data-src="/imgs/heros/la-puerta-del-sol-cover-3.jpg" class="lazy d-block w-100" style="max-height: 800px" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="carousel-item">
        <img data-src="/imgs/heros/la-puerta-del-sol-cover-4.jpg" class="lazy d-block w-100" style="max-height: 800px" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="carousel-item">
        <img data-src="/imgs/heros/la-puerta-del-sol-cover-5.jpg" class="lazy d-block w-100" style="max-height: 800px" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
    </div>
  </div>
</div>
