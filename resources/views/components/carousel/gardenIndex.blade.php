<div id="carouselGardenIndex" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/news/news-new-garden-entrance.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-heror.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-2r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-3r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-4r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-5r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-6r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-7r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-8r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-9r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/bar/the-garden-bar-hero-10r.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
  </div>
</div>
