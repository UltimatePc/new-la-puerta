<div id="carouselRoomJunior" class="carousel slide carousel-fade" data-interval="false" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselRoomJunior" data-slide-to="0" class="active"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="1"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="2"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="3"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="4"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="5"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="6"></li>
    <li data-target="#carouselRoomJunior" data-slide-to="7"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-1.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-2.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-3.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-4.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-5.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-6.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-7.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-j-suite-8.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselRoomJunior" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselRoomJunior" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
