<div id="carouselRoomStandard" class="carousel slide carousel-fade" data-interval="false" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselRoomStandard" data-slide-to="0" class="active"></li>
    <li data-target="#carouselRoomStandard" data-slide-to="1"></li>
    <li data-target="#carouselRoomStandard" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/heros/la-puerta-standard.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-bathroom.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-standard-2.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselRoomStandard" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselRoomStandard" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
