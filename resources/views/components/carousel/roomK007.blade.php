<div id="carouselRoomK007" class="carousel slide carousel-fade" data-interval="false" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselRoomK007" data-slide-to="0" class="active"></li>
    <li data-target="#carouselRoomK007" data-slide-to="1"></li>
    <li data-target="#carouselRoomK007" data-slide-to="2"></li>
    <li data-target="#carouselRoomK007" data-slide-to="3"></li>
    <li data-target="#carouselRoomK007" data-slide-to="4"></li>
    <li data-target="#carouselRoomK007" data-slide-to="5"></li>
    <li data-target="#carouselRoomK007" data-slide-to="6"></li>
    <li data-target="#carouselRoomK007" data-slide-to="7"></li>
    <li data-target="#carouselRoomK007" data-slide-to="8"></li>
    <li data-target="#carouselRoomK007" data-slide-to="9"></li>
    <li data-target="#carouselRoomK007" data-slide-to="10"></li>
    <li data-target="#carouselRoomK007" data-slide-to="11"></li>
    <li data-target="#carouselRoomK007" data-slide-to="12"></li>
    <li data-target="#carouselRoomK007" data-slide-to="13"></li>
    <li data-target="#carouselRoomK007" data-slide-to="14"></li>
    <li data-target="#carouselRoomK007" data-slide-to="15"></li>
    <li data-target="#carouselRoomK007" data-slide-to="16"></li>
    <li data-target="#carouselRoomK007" data-slide-to="17"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/heros/k-suite-5.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-14.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-3.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-4.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-6.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-7.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-8.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-9.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-10.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-11.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-12.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-13.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-2.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-15.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-16.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/k-suite-17.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselRoomK007" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselRoomK007" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
