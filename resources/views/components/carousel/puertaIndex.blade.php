<div id="carouselPuertaIndex" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/heros/la-puerta-del-sol-hotel.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-hotel-2.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-hotel-3.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-hotel-4.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/la-puerta-del-sol-hotel-5.jpg" class="lazy d-block w-100" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
    </div>
  </div>
</div>
