<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-primary">
  <img class="d-block d-lg-none" src="/imgs/logos/la-puerta-del-sol-hotel-logo-2.png" style="height: 50px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <ul class="navbar-nav text-center d-flex align-items-center mx-auto pl-md-5">
      <li class="nav-item{{ isset($active) && $active == 'Home' ? ' active' : ''}}">
        <a class="nav-link text-white" href="{{ route('index') }}">Home</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Rooms' ? ' active' : ''}}">
        <a class="nav-link text-white" href="{{ route('rooms') }}">Rooms</a>
      </li>
      <a class="navbar-brand mx-5 d-none d-sm-block" href="{{ route('index') }}">
        <img src="/imgs/logos/la-puerta-del-sol-hotel-logo-2.png" height="50px" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </a>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Extras
        </a>
        <div class="dropdown-menu text-white text-center text-lg-left" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item{{ isset($active) && $active == 'News' ? ' active' : ''}}" href="{{ route('news') }}">News</a>
          <a class="dropdown-item{{ isset($active) && $active == 'Activities' ? ' active' : ''}}" href="{{ route('activities') }}">Activities</a>
          <a class="dropdown-item{{ isset($active) && $active == 'About Us' ? ' active' : ''}}" href="{{ route('aboutUs') }}">About Us</a>
          <a class="dropdown-item{{ isset($active) && $active == 'Contact Us' ? ' active' : ''}}" href="{{ route('contactUs') }}">Contact Us</a>
          <a class="dropdown-item{{ isset($active) && $active == 'Suggestion' ? ' active' : ''}}" href="{{ route('suggestion') }}">Submit a Suggestion</a>
        </div>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Gallery' ? ' active' : ''}}">
        <a class="nav-link text-white" href="{{ route('gallery') }}">Gallery</a>
      </li>
      <!-- <li class="nav-item{{ isset($active) && $active == 'Contact Us' ? ' active' : ''}}">
        <a class="nav-link text-white" href="https://lapuertadelsol.client.innroad.com/">Book Now</a>
      </li> -->
      <li class="ml-md-3 nav-item{{ isset($active) && $active == 'Contact Us' ? ' active' : ''}}">
        <a class="nav-link text-center float-right" href="@yield('langSwitch')"><button type="button" class="btn btn-light">Español</button></a>
      </li>
    </ul>
  </div>
</nav>
