@extends('layouts.guest')

@section('title', 'Activities')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Activities')
@section('langSwitch', '/actividades')

@include('components.navbar', ['active' => 'Activities'])

@section('content')

<div class="container-fluid">
  <div class="row text-center my-5">
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/golf-cart-rental.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">Golf Cart Rentals</h1>
      <p>
        While you visit and enjoy our amazing garden reservoir, escape to enjoy playas del coco in your very own golf-cart!
        We offer a daily rental ($25) and weekly rental ($125) so you may take the experience of Costa Rica even further traveling along the beach side!
      </p>
    </div>
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/deep-blue-diving.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">Scuba Diving</h1>
      <p>
        See the exotic sea life from underwater, ask about our diving activities
      </p>
      <a href="http://www.deepblue-diving.com/blog/2019/07/21/hotel-la-puerta-del-sol/" class="btn btn-primary" role="button">
        Click Here
      </a>
    </div>
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/wednesday-sunset-market.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">Coco Sunset Market</h1>
      <p>
        Join us <b> <i> Wednesday from 4pm - 8pm </i> </b> for a chill sunset and drink specials while touring around local artisan items!
      </p>
    </div>
  </div>
  <div class="row text-center">
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/padi-travel.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">PADI Travel</h1>
      <p>
        Enjoy many wonderful areas and places that Costa Rica has to visit by joining our travelers on adventures
      </p>
      <a href="https://travel.padi.com/dive-resort/costa-rica/hotel-la-puerta-del-sol/" class="btn btn-primary" role="button">
        Click Here
      </a>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark text-white">
          <img data-src="/imgs/activities/thirsty-thursday.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        </div>
      <h1 class="my-3">Live Music on our Thirsty Thursdays</h1>
      <p>
        Come see many of the local artist that come out to give you the "Pura Vida" vibe while sitting back enjoying a cold drink
      </p>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark text-white">
          <img data-src="/imgs/activities/coco-sunday-market.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        </div>
      <h1 class="my-3">Coco Sunday Market</h1>
      <p>
        Join us <b> <i> Sunday from 10am - 2pm </i> </b> to enjoy a nice walk in the sun while holding bottomless mimosas around La Puerta del Sol, come see stands put up by locals!
      </p>
    </div>
  </div>
</div>

@endsection

@section('modals')

@endsection
