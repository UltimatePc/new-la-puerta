@extends('layouts.guest')

@section('title', 'Sobre Nosotros')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Sobre Nosotros')
@section('langSwitch', '/aboutUs')


@section('content')

@include('components.esNavbar', ['active' => 'Sobre Nosotros'])

<div class="container-fluid">

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">Sobre La Puerta Del Sol</h1>
  </div>

  <div class="row">
    <div class="col-12 d-flex align-items-center px-0">
      <div class="container">
        <h1>Estado de la misión:</h1>
        <p>
          Nuestra misión es proporcionar a nuestros huéspedes una hospitalidad personalizada de clase mundial para garantizar el escapismo absoluto en <b> La Puerta del Sol </b>.
        </p>
        <h2>El enfoque:</h2>
        <p>
          La Puerta del Sol se esfuerza por proporcionar excelentes instalaciones de alojamiento y servicios a nuestros huéspedes.
          Nuestro hotel se enfoca en crear un ambiente tranquilo con nuestro área de jardín y piscina y enfatizamos eso en cómo
          La Puerta del Sol no es solo un hotel sino un santuario para alcanzar la relajación absoluta.
        </p>
        <p>
          Nuestro objetivo es inspirar a los viajeros a acercarse a su entorno, aventurarse más allá de lo común y ver el mundo como fascinante.
          Lo que trae a la gente de regreso a La Puerta del Sol son sus tranquilos terrenos. Nos enfocamos en mantener nuestro jardín en perfectas condiciones diariamente.
        </p>
        <p>
          Alentamos a nuestros huéspedes a acercarse realmente a su entorno cuando se quedan con nosotros.
          Nuestra misión como equipo en La Puerta del Sol es proporcionar a nuestros huéspedes una hospitalidad personalizada de clase mundial para garantizar un escapismo absoluto en nuestro hotel.
        </p>
      </div>
    </div>

  </div>
</div>

@endsection
