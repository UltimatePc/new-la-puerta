@extends('layouts.guest')

@section('title', 'Contact Us')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Contact Us')
@section('langSwitch', '/contactUs')


@section('content')

@include('components.esNavbar', ['active' => 'Contact Us'])

<div class="container-fluid">

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">Información de contacto y ubicación</h1>
  </div>

  <p class="text-center">Gerente/Propietario: Devon P</p>
  <div class="row">
    <div class="col-12 col-lg-6 d-flex align-items-center px-0">
      <div class="container px-0 text-center mx-auto">
        <img class="lazy my-5" data-src="/imgs/logos/la-puerta-del-sol-hotel-logo.png" style="height: 60px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/phone.png"> - (+506) 2670-0195
        </p>
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/sms.png"> - (+506) 8542-5670
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/hotel.lapuertadelsol/">Haga clic aquí</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/hotel.lapuertadelsol/">Facebook.com/hotel.lapuertadelsol/</a>
        </p>
        <p class="d-block d-lg-none text-primary">
        <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/filled-message.png"> - <a href="mailto:lapuertadelsolcostarica@gmail.com">Haga clic aquí</a>
        </p>
        <p class="d-none d-lg-block text-primary">
        <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/filled-message.png"> - <a href="mailto:lapuertadelsolcostarica@gmail.com">lapuertadelsolcostarica@gmail.com</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55092230%2C-85.69387370&navigate=yes&zoom=16">Waze Orientación e indicaciones</a>
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55092230%2C-85.69387370&navigate=yes&zoom=16">Haga clic aquí</a>
        </p>
      </div>
    </div>

    <div class="col-12 col-lg-6 d-flex align-items-center px-0">
      <div class="container px-0 text-center mx-auto">
        <img class="lazy my-5" data-src="/imgs/logos/the-garden-bar.png" style="height: 80px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/phone.png"> - (+506) 2670-0195
        </p>
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/sms.png"> - (+506) 8542-5670
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/thegardenbar.cr/">Haga clic aquí</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/thegardenbar.cr/">Facebook.com/thegardenbar.cr/</a>
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/instagram-new.png"> - <a href="https://www.instagram.com/the_garden_bar_cr/">Haga clic aquí</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/instagram-new.png"> - <a href="https://www.instagram.com/the_garden_bar_cr/">Instagram.com/the_garden_bar_cr/</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55104890%2C-85.69395950&navigate=yes&zoom=16">Waze Orientación e indicaciones</a>
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55104890%2C-85.69395950&navigate=yes&zoom=16">Haga clic aquí</a>
        </p>
      </div>
    </div>

    <div class="container">
      <div class="col-12 mt-5">
        <iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.361922348988!2d-85.69618438470752!3d10.550825466201125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f9e298359bbf475%3A0x41ec5ec6d4a6803e!2sHotel+Puerta+del+Sol!5e0!3m2!1sen!2scr!4v1549578066245" class="lazy pb-5" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

@endsection
