@extends('layouts.guest')

@section('title', 'Home')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Home')
@section('langSwitch', '/es')


@section('content')

@include('components.navbar', ['active' => 'Home'])

<div class="container-fluid">

  <div class="row">
    <div class="col-12 px-0 mx-0">
      @include('components/carousel.heroIndex')
      <div class="overlay d-flex justify-content-center align-items-end">
        <img class="lazy mb-3 d-none d-lg-block" data-src="/imgs/logos/la-puerta-del-sol-hotel-logo-2.png" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
    </div>
  </div>

  <div class="row d-flex justify-content-center align-items-center border-bottom py-lg-5 my-lg-5">
    <div class="col-12 col-lg-6 d-flex align-items-center px-0 my-5">
      <div class="container text-center">
        <img class="lazy mb-5" data-src="/imgs/logos/la-puerta-del-sol-hotel-logo.png" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div id="carouselContent" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center px-lg-5">
              <p>
                La Puerta del Sol Hotel is a great starting point to begin your adventure in Costa Rica.
                Located in Playas del Coco, Guanacaste, Hotel La Puerta del Sol provides a convenient location to access some of the great activities Costa Rica offers.
              </p>
              <p class="d-none d-lg-block">
                We are approximately 25 km away from the international airport of Liberia in the heart of the Papagayo Bay.
                La Puerta del Sol presents a natural oasis with its beautiful quiet gardens and vibrant layers of vegetation.
                The pool and The Garden Bar area is the perfect place to return to from a days activity and enjoy a cold beverage.
              </p>
              <p class="d-none d-lg-block px-lg-4">
                We are less than a five minute walk to the beach and center street where many of the shops and restaurants can be found.
                When looking for hotels in Coco, Costa Rica look no further then our “hidden gem”.
              </p>
              <a href="#carouselContent" role="button" data-slide="next">
                <button type="button" class="btn btn-primary">Watch Video</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <div class="container">
                <iframe data-aos="zoom-in" class="lazy" data-src="https://www.youtube.com/embed/zyWw0EPbShE?rel=0&amp;autoplay=0&mute=1&showinfo=0" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
              </div>
              <a href="#carouselContent" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">About Hotel</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-6 px-0 my-5 text-center">
      @include('components/carousel.puertaIndex')
      <p class="d-block d-lg-none mt-5 px-3">
        We are approximately 25 km away from the international airport of Liberia in the heart of the Papagayo Bay.
        La Puerta del Sol presents a natural oasis with its beautiful quiet gardens and vibrant layers of vegetation.
        The pool and The Garden Bar area is the perfect place to return to from a days activity and enjoy a cold beverage.
      </p>
      <p class="d-block d-lg-none px-3">
        We are less than a five minute walk to the beach and center street where many of the shops and restaurants can be found.
        When looking for hotels in Coco, Costa Rica look no further then our “hidden gem”.
      </p>
    </div>
  </div>

  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0 my-5">
      @include('components/carousel.gardenIndex')
    </div>
    <div class="col-12 col-lg-6 d-flex align-items-center px-0">
      <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
          <img class="lazy my-5 d-block d-lg-none" data-src="/imgs/logos/the-garden-bar.png" style="height: 125px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          <img class="lazy my-5 d-none d-lg-block" data-src="/imgs/logos/the-garden-bar-logo-1.png" style="height: 250px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        </div>
        <div class="row">
          <div class="text-dark text-center px-5 my-5">
            <p>
              Come visit The Garden Bar in Playas del Coco
            </p>
            <p>
              Kick back and relax in our tropical-style garden that gives you a profound feeling with nature while having a cold drink located in our hotel! In Guanacaste, Costa Rica, Playas Del Coco
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">Contact & Location Info</h1>
  </div>

  <p class="text-center">Manager: Gerardo Medina V.</p>
  <p class="text-center">Owner: Devon Pickering</p>
  <div class="row">
    <div class="col-12 col-lg-6 d-flex align-items-center px-0">
      <div class="container px-0 text-center mx-auto">
        <img class="lazy my-5" data-src="/imgs/logos/la-puerta-del-sol-hotel-logo.png" style="height: 60px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/phone.png"> - (+506) 2670-0195
        </p>
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/sms.png"> - (+506) 8542-5670
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/hotel.lapuertadelsol/">Click Here</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/hotel.lapuertadelsol/">Facebook.com/hotel.lapuertadelsol/</a>
        </p>
        <p class="d-block d-lg-none text-primary">
        <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/filled-message.png"> - <a href="mailto:lapuertadelsolcostarica@gmail.com">Click Here</a>
        </p>
        <p class="d-none d-lg-block text-primary">
        <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/filled-message.png"> - <a href="mailto:lapuertadelsolcostarica@gmail.com">lapuertadelsolcostarica@gmail.com</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55092230%2C-85.69387370&navigate=yes&zoom=16">Waze Guidance and Directions</a>
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55092230%2C-85.69387370&navigate=yes&zoom=16">Click Here</a>
        </p>
      </div>
    </div>

    <div class="col-12 col-lg-6 d-flex align-items-center px-0">
      <div class="container px-0 text-center mx-auto">
        <img class="lazy my-5" data-src="/imgs/logos/the-garden-bar.png" style="height: 80px;" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/phone.png"> - (+506) 2670-0195
        </p>
        <p>
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/sms.png"> - (+506) 8542-5670
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/thegardenbar.cr/">Click Here</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png"> - <a href="https://www.facebook.com/thegardenbar.cr/">Facebook.com/thegardenbar.cr/</a>
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/instagram-new.png"> - <a href="https://www.instagram.com/the_garden_bar_cr/">Click Here</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/instagram-new.png"> - <a href="https://www.instagram.com/the_garden_bar_cr/">Instagram.com/the_garden_bar_cr/</a>
        </p>
        <p class="d-none d-lg-block text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55104890%2C-85.69395950&navigate=yes&zoom=16">Waze Guidance and Directions</a>
        </p>
        <p class="d-block d-lg-none text-primary">
          <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;"> - <a href="https://www.waze.com/ul?ll=10.55104890%2C-85.69395950&navigate=yes&zoom=16">Click Here</a>
        </p>
      </div>
    </div>

    <div class="container">
      <div class="col-12 mt-5">
        <iframe data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.361922348988!2d-85.69618438470752!3d10.550825466201125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f9e298359bbf475%3A0x41ec5ec6d4a6803e!2sHotel+Puerta+del+Sol!5e0!3m2!1sen!2scr!4v1549578066245" class="lazy pb-5" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>

    <div class="col-12 mt-5 px-0 text-center mx-auto">
      <p>Please click download button to download new safety protocols</p>
      <a  download="Protocols.pdf" href="/docs/Protocols.pdf">
        <button type="button" class="btn btn-primary my-2">
          Download
        </button>
      </a>
    </div>
  </div>
</div>

@endsection
