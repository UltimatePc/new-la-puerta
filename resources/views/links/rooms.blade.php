@extends('layouts.guest')

@section('title', 'Rooms')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Rooms')
@section('langSwitch', '/habitaciones')

@section('content')

@include('components.navbar', ['active' => 'Rooms'])

<div class="container-fluid pt-lg-1 text-center">

  <!-- K007 Suites -->
  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0 mb-5 mb-lg-0 d-flex justify-content-end">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        K-007 Suite
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#k007SuiteModal">
          More Info
        </button>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#k007SuiteModalV">
          Room Video
        </button>
      </h1>
    </div>
    <div class="col-12 col-lg-6 px-0 order-first order-lg-last">
      @include('components/carousel.roomK007')
    </div>
  </div>

  <!-- Master Suites -->
  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0">
      @include('components/carousel.roomMaster')
    </div>
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-start mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        Master Suites
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#masterSuiteModal">
          More Info
        </button>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#masterSuiteModalV">
          Room Video
        </button>
      </h1>
    </div>
  </div>

  <!-- Junior Suites -->
  <div class="row d-flex align-items-center mb-lg-0">
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-end mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        Junior Suites
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#juniorSuiteModal">
          More Info
        </button>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#juniorSuiteModalV">
          Room Video
        </button>
      </h1>
    </div>
    <div class="col-12 col-lg-6 px-0 order-first order-lg-last">
      @include('components/carousel.roomJunior')
    </div>
  </div>


  <!-- Standard Suites -->
  <div class="row d-none justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0">
      @include('components/carousel.roomStandard')
    </div>
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-start mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        Standard Suites
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#standardSuiteModal">
          More Info
        </button>
      </h1>
    </div>
  </div>

  <!-- Economy Suites -->
  <div class="row d-none align-items-center mb-lg-0">
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-end mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        Economy Suites
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#economySuiteModal">
          More Info
        </button>
      </h1>
    </div>
    <div class="col-12 col-lg-6 px-0 order-first order-lg-last">
      @include('components/carousel.roomEconomy')
    </div>
  </div>

</div>

@endsection

@section('modals')

<!-- Modals for Economy Suites -->
<div class="modal fade" id="economySuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableEconomy" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableEconomy">Economy Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomEconomyDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
              <h3>Rates :</h3>
              <p>Current Season : <b>(High Season Rates)</b></p>
              <p>December 1st - April 30th</p>
              <br>
              <p><b>Single - $70.00 + Tax</b></p>
              <p><b>Double - $85.00 + Tax</b></p>
              <p><b>Kids from 6-11 - $8.00</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomEconomyDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Low Season Rates</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Rates :</h3>
              <p>Next Season : <b>(Low Season Rates)</b></p>
              <p>May 1st - November 30th</p>
              <br>
              <p><b>Single - $55.00 + Tax</b></p>
              <p><b>Double - $70.00 + Tax</b></p>
              <p><b>Kids from 6-11 - $8.00</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomEconomyDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Green Season Rates</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Breakfast is included</p>
        <p>- Rates are in U.S. Dollars</p>
        <p>- Children 0 to 5 years old stay free, applies to 1 child per room</p>
        <p>- Children from 6 to 11 pay $8.00 + tax</p>
        <p>- Prepaid</p>
        <b><i>
          Walking distance from the beach and 100 meters from check in and the principle  La Puerta de Sol
        </i></b>
        <p class="mt-3">
          Each room has a complimentary breakfast served at the principle Hotel and has new AC units, 43” Smart TV’s, Refrigerators, Coffee Makers, Free WIFI, Hot Water, and 24/7 security
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Standard Suites -->
<div class="modal fade" id="standardSuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableStandard" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableStandard">Standard Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomStandardDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
              <h3>Rates :</h3>
              <p>Current Season : <b>(High Season Rates)</b></p>
              <p>December 1st - April 30th</p>
              <br>
              <p><b>Single - $100.00 + Tax</b></p>
              <p><b>Double - $115.00 + Tax</b></p>
              <p><b>Extra Person - $15.00 + Tax</b></p>
              <p><b>Kids from 6-11 - $8.00</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomStandardDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Low Season Rates</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Rates :</h3>
              <p>Next Season : <b>(Low Season Rates)</b></p>
              <p>May 1st - November 30th</p>
              <br>
              <p><b>Single - $75.00 + Tax</b></p>
              <p><b>Double - $90.00 + Tax</b></p>
              <p><b>Extra Person - $15.00 + Tax</b></p>
              <p><b>Kids from 6-11 - $8.00</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomStandardDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Green Season Rates</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Breakfast is included</p>
        <p>- Rates are in U.S. Dollars</p>
        <p>- Prices include 2 people, $15.00 + tax per person after first 2</p>
        <p>- Children 0 to 5 years old stay free, applies to 1 child per room</p>
        <p>- Children from 6 to 11 pay $8.00 + tax</p>
        <p>- Prepaid</p>
        <b><i>
          Walking distance from the beach and 100 meters from check in and the principle  La Puerta de Sol
        </i></b>
        <p class="mt-3">
          Each room has a complimentary breakfast served at the principle Hotel and has new AC units, 43” Smart TV’s, Refrigerators, Coffee Makers, Free WIFI, Hot Water, and 24/7 security. These rooms come with an option of double beds and spacious closets
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Junior Suites -->
<div class="modal fade" id="juniorSuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableJunior" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableJunior">Junior Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomJuniorDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
              <h3>Rates :</h3>
              <p>Current Season : <b>(High Season Rates)</b></p>
              <p>December 1st - April 30th</p>
              <br>
              <p><b>Single - $100.00 + Tax</b></p>
              <p><b>Double - $140.00 + Tax</b></p>
              <p><b>Extra Person - $45.00 + Tax</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomJuniorDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Low Season Rates</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Rates :</h3>
              <p>Next Season : <b>(Low Season Rates)</b></p>
              <p>May 1st - November 30th</p>
              <br>
              <p><b>Single - $80.00 + Tax</b></p>
              <p><b>Double - $100.00 + Tax</b></p>
              <p><b>Extra Person - $45.00 + Tax</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomJuniorDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Green Season Rates</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Breakfast is included</p>
        <p>- Rates are in U.S. Dollars</p>
        <p>- Prices include 2 people, $45.00 + tax per person after first 2</p>
        <p>- Children 0 to 5 years old stay free, applies to 1 child per room</p>
        <p>- Children from 6 to 11 pay $22.00 + tax</p>
        <p>- Prepaid</p>
        <p class="my-3">
          *Amenities Include :
          <i class="fas fa-tv"></i> TV and Cable , <i class="far fa-snowflake"></i> New Air-Conditioning Units , <i class="fas fa-wifi"></i> Wifi ,
          <i class="fab fa-hotjar"></i> Hot Water , <i class="fas fa-archive"></i> Mini-Fridge , <i class="fas fa-car"></i> Parking , <i class="fas fa-star"></i> Safe,
          <i class="fas fa-star"></i> Microwave, <i class="fas fa-star"></i> Coffee-Maker, <i class="fas fa-paw"></i> Pet friendly! (requires additional fees)
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Video Junior Suites -->
<div class="modal fade" id="juniorSuiteModalV" tabindex="-1" role="dialog" aria-labelledby="modalLableJuniorV" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableJuniorV">Junior Suites Video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <iframe class="lazy" data-src="https://www.youtube.com/embed/hd8mEkFRa3c?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Master Suites -->
<div class="modal fade" id="masterSuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableMaster" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableMaster">Master Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomMasterDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
              <h3>Rates :</h3>
              <p>Current Season : <b>(High Season Rates)</b></p>
              <p>December 1st - April 30th</p>
              <br>
              <p><b>Single - $130.00 + Tax</b></p>
              <p><b>Double - $160.00 + Tax</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomMasterDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Green Season Rates</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Rates :</h3>
              <p>Next Season : <b>(Low Season Rates)</b></p>
              <p>May 1st - November 30th</p>
              <br>
              <p><b>Single - $110.00 + Tax</b></p>
              <p><b>Double - $140.00 + Tax</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomMasterDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">High Season Rates</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Breakfast is included</p>
        <p>- Rates are in U.S. Dollars</p>
        <p>- Prices include 2 people, $45.00 + tax per person after first 2</p>
        <p>- Children 0 to 5 years old stay free, applies to 1 child per room</p>
        <p>- Children from 6 to 11 pay $22.00 + tax</p>
        <p>- Prepaid</p>
        <p class="my-3">
          *Amenities Include :
          <i class="fas fa-tv"></i> TV and Cable , <i class="far fa-snowflake"></i> New Air-Conditioning Units , <i class="fas fa-wifi"></i> Wifi ,
          <i class="fab fa-hotjar"></i> Hot Water , <i class="fas fa-archive"></i> Mini-Fridge , <i class="fas fa-car"></i> Parking , <i class="fas fa-star"></i> Safe,
          <i class="fas fa-star"></i> Microwave, <i class="fas fa-star"></i> Coffee-Maker, <i class="fas fa-paw"></i> Pet friendly! (requires additional fees)
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Video Master Suites -->
<div class="modal fade" id="masterSuiteModalV" tabindex="-1" role="dialog" aria-labelledby="modalLableMasterV" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableMasterV">Master Suites Video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <div class="container">
            <iframe class="lazy" data-src="https://www.youtube.com/embed/PEAMkmSuTro?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for K-007 Suite -->
<div class="modal fade" id="k007SuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableK007" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableK007">K-007 Suite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomK007Details" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center px-lg-5">
              <h3>Rates :</h3>
              <p>Current Season : <b>(High Season Rates)</b></p>
              <p>December 1st - April 30th</p>
              <br>
              <p><b>Single - $180.00 + Tax</b></p>
              <p><b>Double - $200.00 + Tax</b></p>
              <p><b>Extra Person - $45.00 + Tax</b></p>
              <p><b>(Max 6 Pax)</b></p>
              <a href="#carouselRoomK007Details" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Green Season Rates</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Rates :</h3>
              <p>Next Season : <b>(Low Season Rates)</b></p>
              <p>May 1st - November 30th</p>
              <br>
              <p><b>Single - $120.00 + Tax</b></p>
              <p><b>Double - $160.00 + Tax</b></p>
              <p><b>Extra Person - $45.00 + Tax</b></p>
              <p><b>(Max 6 Pax)</b></p>
              <a href="#carouselRoomK007Details" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">High Season Rates</button>
              </a>
            </div>
          </div>
        </div>
        <p>
          <i>
            Enjoy a quality apartment lifestyle inside the garden of la puerta del sol, this is the luxury suite with 2 bedrooms,
            a Master bed and 2 additional beds, a Efficient kitchen for all your needs, new custom made furniture, smart Tv and more!
          </i>
        </p>
        <p>- Breakfast is included</p>
        <p>- Rates are in U.S. Dollars</p>
        <p>- Prices include 2 people, $45.00 + tax per person after first 2</p>
        <p>- Children 0 to 5 years old stay free, applies to 1 child per room</p>
        <p>- Children from 6 to 11 pay $22.00 + tax</p>
        <p>- Prepaid</p>
        <p class="my-3">
          *Amenities Include :
          <i class="fas fa-tv"></i> TV and Cable , <i class="far fa-snowflake"></i> New Air-Conditioning Units , <i class="fas fa-wifi"></i> Wifi ,
          <i class="fab fa-hotjar"></i> Hot Water , <i class="fas fa-archive"></i> Mini-Fridge , <i class="fas fa-car"></i> Parking , <i class="fas fa-utensils"></i> Efficient Kitchen
          <i class="fas fa-star"></i> Safe, <i class="fas fa-star"></i> Microwave, <i class="fas fa-star"></i> Coffee-Maker, <i class="fas fa-paw"></i> Pet friendly! (requires additional fees)
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Video K-007 Suite -->
<div class="modal fade" id="k007SuiteModalV" tabindex="-1" role="dialog" aria-labelledby="modalLableK007V" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableK007V">K-007 Suite Video</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <div class="container">
            <iframe class="lazy" data-src="https://www.youtube.com/embed/sXbXilpBFTc?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Book Now</button>
        </a>
      </div>
    </div>
  </div>
</div>
