@extends('layouts.guest')

@section('title', 'Gallery')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Gallery')
@section('langSwitch', '/gallery')

@include('components.esNavbar', ['active' => 'Galeria'])

@section('content')


<div class="container-fluid pt-lg-1 text-center">
  <div class="row">
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/BLUE2020/blues-2020-(202).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Evento Blues Festival 2020</h5>
        <p class="card-text">¡El Hotel La Puerta del Sol quisiera agradecer a todos los que pudieron acompañarnos en este excelente día!</p>
        <a href="{{ route('esbluesfest2020') }}">
          <button type="button" class="btn btn-primary">Galería de imágenes</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/CARNIVAL2020/carnival-2020-(228).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Evento Carnival 2020</h5>
        <p class="card-text">¡El Hotel La Puerta del Sol quisiera agradecer a todos los que pudieron acompañarnos en este excelente día!</p>
        <a href="{{ route('escarnival2020') }}">
          <button type="button" class="btn btn-primary">Galería de imágenes</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/PAPA2019/taste-of-papagayo-(49).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Sabor de Papagayo 2019</h5>
        <p class="card-text">El Hotel La Puerta del Sol quisiera agradecer a todos los que hicieron posible este año Taste of Papagayo.
          Restaurantes, nuestro personal y la comunidad, los amamos y apreciamos a todos, no sería posible sin ustedes :) ¡Gracias por todo su apoyo!</p>
        <a href="{{ route('espapagayo2019') }}">
          <button type="button" class="btn btn-primary">Galería de imágenes</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/OCEAN2019/ocean-fest-2019-(120).JPG" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Ocean Fest 2019</h5>
        <p class="card-text">Ocean Fest 8 de junio en el Hotel La Puerta del Sol. Patrocinado por Rich Coast Diving.</p>
        <a href="{{ route('esoceanfest2019') }}">
          <button type="button" class="btn btn-primary">Galería de imágenes</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/PDS2019/leatherback-pds-(104).JPG" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">PDS Leatherbacks 2019</h5>
        <p class="card-text">Donnie Walsh y The Leatherbacks :) Gracias Kimberly Irons por estas increíbles fotos. Qué gran noche en La Puerta del Sol.</p>
        <a href="{{ route('esleatherback2019') }}">
          <button type="button" class="btn btn-primary">Galería de imágenes</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/PAPA2018/taste-of-papagayo-(21).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Sabor de Papagayo 2018</h5>
        <p class="card-text">El Hotel La Puerta del Sol quisiera agradecer a todos los que hicieron posible este año Taste of Papagayo.
          Restaurantes, nuestro personal y la comunidad, los amamos y apreciamos a todos, no sería posible sin ustedes :) ¡Gracias por todo su apoyo!</p>
        <a href="{{ route('espapagayo2018') }}">
          <button type="button" class="btn btn-primary">Galería de imágenes</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/Neotics-made-me-funky.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Neotics - Made me Funky (Funk Fest 2019)</h5>
        <p class="card-text">Actuación en vivo en la puerta del sol de Neotics tocando "Made me Funky"</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#neoticsFunky">Vídeo</button>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/Soul-kitchen-whats-going-on.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Soul Kitchen - What's Going On (Funk Fest 2019)</h5>
        <p class="card-text">Actuación en vivo en la puerta del sol de Soul Kitchen tocando "What's going on?"</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#soulGoingOn">Vídeo</button>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/Soul-kitchen-respect.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Soul Kitchen - Respect (Funk Fest 2019)</h5>
        <p class="card-text">Actuación en vivo en la puerta del sol de Soul Kitchen tocando "Respect"</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#soulRespect">Vídeo</button>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/event-easter.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Evento de Pascua - 2019</h5>
        <p class="card-text">¡Gracias por estar con nosotros en un día tan especial para los niños!</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#easterEvent2019">Vídeo</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('modals')

<div class="modal fade" id="neoticsFunky" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Neotics - Made me funky</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/L8ox2ZRnnYo?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="soulGoingOn" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Soul Kitchen - Whats going on?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/TLEtsShghj4?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="soulRespect" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Soul Kitchen - Respect</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/9jbu7dULL_8?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="easterEvent2019" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Evento de Pascua - 2019</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/IV6lKCnvXa8?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
