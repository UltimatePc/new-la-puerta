@extends('layouts.guest')

@section('title', 'Noticias')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Noticias')
@section('langSwitch', '/news')

@include('components.esNavbar', ['active' => 'Noticias'])

@section('content')

<div class="container-fluid">

  <h1 class="text-center border-bottom mt-5">Noticias de La Puerta del Sol</h1>

  <div class="row">

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-extended-bar.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Área de la segunda barra extendida</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#extendedbarModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">10 de Noviembre de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-new-garden-entrance.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Nueva entrada al Garden Bar</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#entranceNewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">8 de Agosto de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-eco-coco.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Se recaudó un gran total de $ 1,335 para ayudar a ECO-COCO</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#ecoCocoNewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">17 de Julio de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/taste-of-papagayo-2019.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Sabor de Papagayo 2019</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#papagayo2019NewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">13 de Julio de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-monkey-head-craft-beer-2.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">2 nuevos Kegerators de la cerveza artesanal Monkey Head</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#monkeyHeadNewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">11 de Julio de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-momo-craft-beer.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Nueva cerveza artesanal Numu</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#numuNewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">10 de Julio de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card my-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-new-generator.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Nuevo generador instalado</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#generatorNewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">14 de Marzo de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-sol-fest-2019.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Sol-Fest 2019</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#solFest2019">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">9 de Marzo de 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-parking-lot.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Estacionamiento</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#parkingLotNewsModal">
                Lee mas
              </button>
              <p class="card-text"><small class="text-muted">20 de Noviembre de 2018</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection

@section('modals')

<!-- Modals for News Extended Bar -->
<div class="modal fade" id="extendedbarModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsBarExtended" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsBarExtended">Área de la segunda barra extendida</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Con el creciente número de visitantes que se unen a nosotros en nuestros eventos diarios y lugares habituales para pasar el rato,
        Hemos extendido la segunda barra para mantener una mayor capacidad de los que vienen a tomar bebidas.
        ¡Ven a ver por ti mismo cómo hemos empujado la barra más adentro de nuestro jardín!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Entrance -->
<div class="modal fade" id="entranceNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsEntrance" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsEntrance">Nueva entrada al bar del jardín</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">8 de Agosto de 2019</small></p>
        ¡Nos complace anunciar la entrada oficial al bar del jardín!
        ¡Nos encanta el trabajo que nos ha brindado Carlos Hiller y nos encantaría que lo viera usted mismo! <br> <br>
        Ven y disfruta de una buena bebida fría mientras disfrutas del evento del día.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Eco-coco Funraiser -->
<div class="modal fade" id="ecoCocoNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsEcoCoco" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsEcoCoco">Se recaudaron $ 1,335 para ayudar a Eco-Coco</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">17 de Julio de 2019</small></p>
        El Hotel La Puerta del Sol se complace en anunciar que hemos ayudado a recaudar un total de $ 1135.00 y que Tank Tops y Flip Flops recaudaron $ 200 con un gran total de $ 1,335
        de Taste of Papagayo 2019 !!!! <br> <br>
        Esto ayudará a Eco-coco a continuar sus esfuerzos para educar a la comunidad sobre la importancia de respetar,
        proteger y preservar nuestros recursos naturales y convivir con la naturaleza. <br> <br>
        Gracias nuevamente por parte de todos los que nos unimos para hacer de este Taste of Papagayo un gran éxito, apreciamos todo su apoyo.
        ¡Visite http://eco-coco.org para obtener más información para ayudar a esta gran causa!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Papagayo 2019 -->
<div class="modal fade" id="papagayo2019NewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsPapagayo2019" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsPapagayo2019">Sabor de Papagayo 2019</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">13 de Julio de 2019</small></p>
        <div class="container my-5">
          <iframe class="lazy" data-src="https://www.youtube.com/embed/6i39p-4NP_M" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
        </div>
        El Hotel La Puerta del Sol quisiera agradecer a todos los que hicieron posible este año Taste of Papagayo. Restaurantes, nuestro personal y el
        comunidad, los amamos y apreciamos a todos, no sería posible sin ustedes :) ¡Gracias por todo su apoyo!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Monkey Head Keg -->
<div class="modal fade" id="monkeyHeadNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsMonkeyHead" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsMonkeyHead">Nueva cerveza de Monkey Head</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">11 de Julio de 2019</small></p>
        ¡Nos complace anunciar que tenemos cerveza artesanal de cerveza fría Monkey Head ahora en La Puerta del Sol! <br> <br>
        Instalamos 2 kegerators de grifo: <br> <br>
        <b> MANGO Blonde </b>: Piensa localmente, esta cerveza tiene mango en el olor y en el sabor. Es una cerveza ligera muy amarga muy baja. ¡Visítanos hoy y prueba esta nueva y sabrosa cerveza artesanal de barril! <br> <br>
        <b> Ale dorada de lúpulo seco </b>: esta cerveza tiene un olor a cítricos y tiene un sabor suave.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Monkey Head Keg -->
<div class="modal fade" id="numuNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsNumu" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsNumu">Nueva cerveza de Numu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">10 de Julio de 2019</small></p>
        Por favor, anuncie que estaremos vendiendo cerveza Numu a partir de la próxima semana en el Hotel La Puerta del Sol.
        ¡Pasa por una de estas deliciosas cervezas! No puedo tener suficiente, todos son tan buenos
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News New Generator -->
<div class="modal fade" id="generatorNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsGenerator" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsGenerator">Nuevo generador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">14 de Marzo de 2019</small></p>
        Se agregó New Generator en premesis eléctrico para respaldo, Playas del coco ha tenido sus momentos habituales de apagón.
        Por lo tanto, con un nuevo generador, siempre puede contar con la puerta para pasar un buen rato.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Sol Fest 2019 -->
<div class="modal fade" id="solFest2019" tabindex="-1" role="dialog" aria-labelledby="modalSolFest2019" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSolFest2019">Sol Fest 2019</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container my-5">
          <iframe class="lazy" data-src="https://www.youtube.com/embed/VAkBMgiLlZs" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
        </div>
        <p class="card-text mb-3"><small class="text-muted">9 de Marzo de 2019</small></p>
        Solfest fue un festival de música de todo el día con 3 bandas en vivo,
        un asado de cerdo ADEMÁS de que obtienes una cerveza Moosehead gratis o una copa de vino Y las primeras
        200 personas obtienen una camiseta sin mangas "chanclas sin mangas" gratis TODO POR SOLO 6000 colones por 2 boletos !
        Este evento fue patrocinado por MOOSEHEAD beer y Ron Centernario. También se recibió en la puerta un evento de caridad para Patos y Manos.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Parking Lot -->
<div class="modal fade" id="parkingLotNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsParkingLot" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsParkingLot">Nuevo estacionamiento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">8 de Agosto de 2019</small></p>
        La puerta ha comprado el lote al frente, aumentando el tamaño de la capacidad de todos nuestros invitados muy bienvenidos.
        Antes de que este lote estuviera desierto y abandonado, y después de un cuidado tierno y amoroso, ahora tenemos un excelente estacionamiento adicional.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
