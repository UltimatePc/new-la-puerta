@extends('layouts.guest')

@section('title', 'Actividades')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Actividades')
@section('langSwitch', '/activities')

@include('components.esNavbar', ['active' => 'Actividades'])

@section('content')

<div class="container-fluid">
  <div class="row text-center my-5">
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/golf-cart-rental.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">Alquiler de Carritos de Golf</h1>
      <p>
        Mientras visita y disfruta de nuestro increíble jardín, ¡escápese para disfrutar de las playas del coco en su propio carrito de golf!
        Ofrecemos un alquiler diario ($ 25) y un alquiler semanal ($ 125) para que pueda llevar la experiencia de Costa Rica aún más lejos viajando por el lado de la playa
      </p>
    </div>
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/deep-blue-diving.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">Buceo</h1>
      <p>
        Vea la exótica vida marina desde el agua, pregunte por nuestras actividades de buceo
      </p>
      <a href="http://www.deepblue-diving.com/blog/2019/07/21/hotel-la-puerta-del-sol/" class="btn btn-primary" role="button">
        Clic aquí
      </a>
    </div>
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/wednesday-sunset-market.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">Coco Sunset Market</h1>
      <p>
        Únase a nosotros <b> <i> miércoles de 4:00 p. M. A 8:00 p. M. </i> </b> para disfrutar de una puesta de sol fría y bebidas especiales mientras recorre artículos artesanales locales.
      </p>
    </div>
  </div>
  <div class="row text-center">
    <div class="col-12 col-md-4">
      <div class="card bg-dark text-white">
        <img data-src="/imgs/activities/padi-travel.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <h1 class="my-3">PADI Travel</h1>
      <p>
        Disfrute de muchas áreas y lugares maravillosos que Costa Rica tiene que visitar uniéndose a nuestros viajeros en aventuras.
      </p>
      <a href="https://travel.padi.com/dive-resort/costa-rica/hotel-la-puerta-del-sol/" class="btn btn-primary" role="button">
        Clic aquí
      </a>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark text-white">
          <img data-src="/imgs/activities/thirsty-thursday.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        </div>
      <h1 class="my-3">Música en vivo los Jueves Sedientos</h1>
      <p>
        Ven a ver a muchos de los artistas locales que vienen a brindarte el ambiente de "Pura Vida" mientras disfrutas de una bebida fría.
      </p>
    </div>
    <div class="col-12 col-md-4">
        <div class="card bg-dark text-white">
          <img data-src="/imgs/activities/coco-sunday-market.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        </div>
      <h1 class="my-3">Coco Sunday Market</h1>
      <p>
        Únase a nosotros el <b> <i> domingo de 10 a. M. - 2 p. M. </i> </b> para disfrutar de un agradable paseo bajo el sol mientras sostiene mimosas sin fondo alrededor de La Puerta del Sol, ¡venga a ver los puestos montados por los lugareños!
      </p>
    </div>
  </div>
</div>

@endsection

@section('modals')

@endsection
