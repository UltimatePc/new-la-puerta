@extends('layouts.guest')

@section('title', 'News')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'News')
@section('langSwitch', '/noticias')

@include('components.navbar', ['active' => 'News'])

@section('content')

<div class="container-fluid">

  <h1 class="text-center border-bottom mt-5">La Puerta News</h1>

  <div class="row">

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-extended-bar.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Second Bar area extended</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#extendedbarModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">November 10th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-new-garden-entrance.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">New Garden Bar entrance</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#entranceNewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">August 8th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-eco-coco.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">A grand total of $1,335 was raised to help ECO-COCO</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#ecoCocoNewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">July 17th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/taste-of-papagayo-2019.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Taste of Papagayo 2019</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#papagayo2019NewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">July 13th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-monkey-head-craft-beer-2.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">2 New Kegerators of<br> Monkey Head Craft Beer</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#monkeyHeadNewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">July 11th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-momo-craft-beer.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">New Numu Craft Beer</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#numuNewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">July 10th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card my-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-new-generator.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">New Generator Installed</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#generatorNewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">March 14th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-sol-fest-2019.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Sol-Fest 2019</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#solFest2019">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">March 9th, 2019</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-4 my-3 d-flex justify-content-center">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4 d-flex align-items-center">
            <img data-src="/imgs/news/news-parking-lot.jpg" class="lazy card-img" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8">
            <div class="card-body">
              <h5 class="card-title">Parking Lot</h5>
              <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#parkingLotNewsModal">
                Read More
              </button>
              <p class="card-text"><small class="text-muted">November 20th, 2018</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection

@section('modals')

<!-- Modals for News Extended Bar -->
<div class="modal fade" id="extendedbarModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsBarExtended" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsBarExtended">Second Bar area extended</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        With the increasing number of visitor joining us on our daily events and usual hang out spot,
        we have extended the second bar to hold a higher capacity of those that come for drinks.
        Come see for yourself how we have push the bar futher inside our garden!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Entrance -->
<div class="modal fade" id="entranceNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsEntrance" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsEntrance">New Garden Bar Entrance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        We are pleased to announce the official entrance to the garden bar!
        We love the work that Carlos Hiller has provided to us and would love to have you see it for yourself!<br><br>
        Come by and enjoy a nice cold drink while enjoying the event of the day.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Eco-coco Funraiser -->
<div class="modal fade" id="ecoCocoNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsEcoCoco" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsEcoCoco">$1,335 was raised to aid Eco-Coco</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Hotel La Puerta Del Sol is pleased to announce that we helped raise a total of $1135.00 and Tank Tops and Flip Flops raised $200 with a grand total of $1,335
        from Taste of Papagayo 2019 !!!!<br><br>
        This will help Eco-coco to continue their efforts to educate the community about the importance of respecting,
        protecting and preserving our natural resources and living together with nature.<br><br>
        Thank you again from all of us that came together to make this Taste of Papagayo a huge success, we appreciate all your support.
        Please visit http://eco-coco.org for more information to help this great cause !!!!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Papagayo 2019 -->
<div class="modal fade" id="papagayo2019NewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsPapagayo2019" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsPapagayo2019">Taste of Papagayo 2019</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container my-5">
          <iframe class="lazy" data-src="https://www.youtube.com/embed/6i39p-4NP_M" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
        </div>
        Hotel La Puerta Del Sol would like to say Thank you to everyone that made this years Taste of Papagayo possible! Restaurants, our staff, and the
        community, we love and appreciate all of you, it wouldn't be possible without you :) Thank you for all your support!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Monkey Head Keg -->
<div class="modal fade" id="monkeyHeadNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsMonkeyHead" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsMonkeyHead">New Monkey Head Craft Beer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">July 11th, 2019</small></p>
        We are pleased to announce we have Monkey Head cold brew craft beer now at La Puerta Del Sol!<br><br>
        We installed 2 tap kegerators:<br><br>
        <b>MANGO Blonde</b>: Think local, this beer has mango in the smell and in the taste. It is a light ale very low bitterness. Please stop by today and try this new tasty craft beer on tap!<br><br>
        <b>Dry Hopped Golden Ale</b>: This ale has a citrus forward smell and is smooth tasting.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Monkey Head Keg -->
<div class="modal fade" id="numuNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsNumu" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsNumu">New Numu Craft Beer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">July 10th, 2019</small></p>
        Please to announce we will be selling Numu beer starting next week at Hotel La Puerta Del Sol . Please stop over for one of these delicious beers ! Can't get enough they are all so good
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News New Generator -->
<div class="modal fade" id="generatorNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsGenerator" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsGenerator">New Generator</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">March 14th, 2019</small></p>
        New Generator was added on premesis electrical for back-up, Playas del coco have had their usual black out moments. Therefore with a new generator, you can always count on la puerta for a good time!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Sol Fest 2019 -->
<div class="modal fade" id="solFest2019" tabindex="-1" role="dialog" aria-labelledby="modalSolFest2019" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalSolFest2019">Sol Fest 2019</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container my-5">
          <iframe class="lazy" data-src="https://www.youtube.com/embed/VAkBMgiLlZs" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
        </div>
        <p class="card-text mb-3"><small class="text-muted">March 9th, 2019</small></p>
        Solfest was an All Day Music Festival with 3 live bands, a pig roast PLUS you get a free Moosehead beer or glass of wine AND the first 200 people get a free "tank top flip flops" tank top ALL FOR ONLY 6,000 colones for 2 tickets !
        This event was sponsored by MOOSEHEAD beer and Ron Centernario. Also charity event for Patos y Manos was received at the door.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modals for News Parking Lot -->
<div class="modal fade" id="parkingLotNewsModal" tabindex="-1" role="dialog" aria-labelledby="modalLableNewsParkingLot" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableNewsParkingLot">New Parking Lot</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="card-text mb-3"><small class="text-muted">August 8th, 2019</small></p>
        La puerta has purchased the lot out in front, increasing capacity size for all of our very welcomed guest. Before this lot was deserted and abandoned, but and after some tender loving care, we now have a great additional parking.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
