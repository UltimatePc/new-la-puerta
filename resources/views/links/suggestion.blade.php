@extends('layouts.guest')

@section('title', 'Suggestion')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Suggestion')
@section('langSwitch', '/sugerencia')

@section('content')

@include('components.navbar', ['active' => 'Suggestion'])

<div class="container-fluid pt-1">
  <div class="row">
    <div class="col">
      <div class="container mt-5">
        <h1 class="pt-5">Suggestion Form</h1>
        @if (Session::has('flash_message'))
        <div class="alert alert-success"> {{ Session::get('flash_message') }} </div>
        @endif
        <form method="post" action="{{ route('suggestion.store')}}">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="float-left">Full Name: </label>
            <input type="text" name="name" class="form-control">
            @if ($errors->has('name'))
            <small class="form-text invalid-feedback">{{ $errors->first('name') }}</small>
            @endif
          </div>
          <div class="form-group">
            <label class="float-left">Email: </label>
            <input type="text" name="email" class="form-control">
            @if ($errors->has('email'))
            <small class="form-text invalid-feedback">{{ $errors->first('email') }}</small>
            @endif
          </div>
          <div class="form-group">
            <label class="float-left">Your Suggestion: </label>
            <textarea name="message" class="form-control"></textarea>
            @if ($errors->has('message'))
            <small class="form-text invalid-feedback">{{ $errors->first('message') }}</small>
            @endif
          </div>
          <button class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
