@extends('layouts.guest')

@section('title', 'PDS Leatherbacks')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'PDS Leatherbacks')
@section('langSwitch', '/gallery/esleatherback2019')


@section('content')

@include('components.navbar', ['active' => 'Gallery'])

<div class="container-fluid">

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">PDS Leatherbacks</h1>
  </div>

  <div class="row mx-auto">
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(2).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(2).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(3).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(3).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(5).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(5).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(7).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(7).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(8).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(8).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(9).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(9).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(10).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(10).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(11).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(11).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(12).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(12).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(17).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(17).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(18).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(18).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(19).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(19).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(20).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(20).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(22).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(22).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(23).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(23).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(24).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(24).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(26).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(26).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(28).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(28).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(29).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(29).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(30).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(30).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(31).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(31).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(34).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(34).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(36).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(36).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(37).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(37).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(38).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(38).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(39).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(39).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(40).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(40).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(41).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(41).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(42).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(42).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(43).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(43).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(45).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(45).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(46).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(46).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(47).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(47).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(48).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(48).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(49).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(49).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(50).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(50).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(51).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(51).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(52).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(52).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(55).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(55).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(56).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(56).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(57).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(57).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(58).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(58).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(59).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(59).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(60).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(60).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(63).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(63).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(64).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(64).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(65).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(65).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(67).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(67).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(68).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(68).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(69).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(69).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(70).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(70).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(71).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(71).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(72).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(72).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(73).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(73).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(74).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(74).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(75).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(75).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(77).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(77).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(78).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(78).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(79).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(79).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(80).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(80).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(83).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(83).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(84).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(84).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(85).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(85).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(86).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(86).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(87).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(87).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(88).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(88).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(89).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(89).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(90).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(90).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(91).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(91).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(92).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(92).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(93).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(93).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(95).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(95).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(96).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(96).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(97).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(97).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(98).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(98).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(99).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(99).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(100).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(100).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(101).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(101).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(102).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(102).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(103).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(103).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(104).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(104).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(105).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(105).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(109).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(109).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(110).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(110).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(111).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(111).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(112).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(112).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(113).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(113).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(114).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(114).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(116).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(116).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(117).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(117).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(118).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(118).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(119).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(119).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(1).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(1).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(33).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(33).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(35).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(35).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(44).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(44).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(53).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(53).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(62).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(62).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(66).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(66).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(81).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(81).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(82).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(82).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PDS2019/leatherback-pds-(94).JPG" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="PDS-Leatherbacks.jpg" href="/imgs/events/PDS2019/leatherback-pds-(94).JPG">
            <button type="button" class="btn btn-primary my-2">
              Download
            </button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
