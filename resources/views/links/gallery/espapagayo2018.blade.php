@extends('layouts.guest')

@section('title', 'Taste of Papagayo 2018')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Taste of Papagayo 2018')
@section('langSwitch', '/gallery/papagayo2018')


@section('content')

@include('components.esNavbar', ['active' => 'Galeria'])

<div class="container-fluid">

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">Taste of Papagayo 2018</h1>
  </div>

  <div class="row mx-auto">
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(0).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(0).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(1).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(1).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(2).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(2).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(3).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(3).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(4).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(4).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(6).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(6).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(7).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(7).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(8).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(8).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(9).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(9).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(10).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(10).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(11).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(11).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(12).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(12).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(13).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(13).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(14).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(14).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(15).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(15).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(16).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(16).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(17).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(17).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(18).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(18).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(19).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(19).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(20).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(20).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(21).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(21).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(22).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(22).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(24).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(24).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(25).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(25).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(26).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(26).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(27).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(27).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(28).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(28).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(29).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(29).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(30).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(30).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(31).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(31).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(32).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(32).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(33).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(33).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(34).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(34).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(36).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(36).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(37).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(37).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(38).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(38).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(39).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(39).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(41).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(41).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(42).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(42).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(43).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(43).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(44).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(44).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(45).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(45).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(46).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(46).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(47).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(47).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(48).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(48).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(49).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(49).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(50).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(50).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(51).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(51).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(52).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(52).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(54).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(54).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(56).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(56).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(57).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(57).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(58).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(58).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(59).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(59).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(60).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(60).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(61).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(61).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(63).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(63).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(64).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(64).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(65).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(65).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(66).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(66).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(67).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(67).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(68).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(68).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(69).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(69).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(70).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(70).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(71).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(71).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(5).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(5).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(23).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(23).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(35).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(35).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(40).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(40).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(53).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(53).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(55).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(55).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/PAPA2018/taste-of-papagayo-(62).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="taste-of-papagayo.jpg" href="/imgs/events/PAPA2018/taste-of-papagayo-(62).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
