@extends('layouts.guest')

@section('title', 'Blues Festival 2020')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Blues Festival 2020')
@section('langSwitch', '/gallery/bluesfest2020')


@section('content')

@include('components.esNavbar', ['active' => 'Galeria'])

<div class="container-fluid">

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">Blues Festival 2020!</h1>
  </div>

  <div class="row mx-auto">
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(1).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(1).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(2).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(2).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(3).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(3).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(4).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(4).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(5).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(5).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(6).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(6).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(7).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(7).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(8).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(8).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(9).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(9).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(10).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(10).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(11).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(11).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(12).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(12).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(13).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(13).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(14).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(14).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(15).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(15).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(16).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(16).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(17).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(17).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(18).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(18).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(19).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(19).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(20).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(20).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(21).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(21).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(22).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(22).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(23).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(23).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(24).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(24).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(25).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(25).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(26).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(26).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(27).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(27).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(28).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(28).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(29).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(29).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(30).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(30).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(31).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(31).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(32).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(32).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(33).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(33).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(34).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(34).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(35).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(35).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(36).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(36).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(37).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(37).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(38).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(38).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(39).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(39).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(40).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(40).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(41).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(41).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(42).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(42).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(43).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(43).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(44).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(44).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(45).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(45).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(46).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(46).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(47).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(47).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(48).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(48).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(49).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(49).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(50).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(50).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(51).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(51).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(52).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(52).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(53).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(53).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(54).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(54).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(55).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(55).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(56).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(56).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(57).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(57).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(58).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(58).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(59).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(59).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(60).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(60).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(61).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(61).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(62).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(62).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(63).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(63).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(64).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(64).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(65).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(65).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(66).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(66).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(67).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(67).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(68).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(68).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(69).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(69).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(70).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(70).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(71).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(71).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(72).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(72).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(73).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(73).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(74).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(74).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(75).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(75).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(76).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(76).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(77).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(77).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(78).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(78).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(79).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(79).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(80).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(80).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(81).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(81).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(82).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(82).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(83).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(83).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(84).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(84).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(85).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(85).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(86).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(86).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(87).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(87).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(88).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(88).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(89).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(89).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(90).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(90).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(91).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(91).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(92).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(92).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(93).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(93).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(94).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(94).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(95).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(95).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(96).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(96).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(97).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(97).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(98).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(98).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(99).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(99).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(100).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(100).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(101).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(101).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(102).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(102).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(103).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(103).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(104).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(104).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(105).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(105).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(106).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(106).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(107).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(107).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(108).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(108).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(109).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(109).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(110).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(110).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(111).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(111).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(112).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(112).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(113).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(113).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(114).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(114).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(115).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(115).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(116).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(116).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(117).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(117).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(118).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(118).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(119).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(119).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(120).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(120).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(121).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(121).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(122).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(122).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(123).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(123).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(124).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(124).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(125).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(125).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(126).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(126).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(127).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(127).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(128).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(128).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(129).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(129).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(130).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(130).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(131).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(131).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(132).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(132).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(133).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(133).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(134).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(134).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(135).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(135).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(136).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(136).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(137).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(137).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(138).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(138).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(139).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(139).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(140).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(140).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(141).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(141).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(142).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(142).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(143).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(143).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(144).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(144).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(145).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(145).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(146).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(146).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(147).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(147).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(148).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(148).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(149).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(149).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(150).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(150).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(151).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(151).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(152).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(152).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(153).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(153).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(154).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(154).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(155).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(155).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(156).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(156).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(157).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(157).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(158).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(158).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(159).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(159).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(160).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(160).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(161).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(161).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(162).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(162).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(163).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(163).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(164).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(164).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(165).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(165).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(166).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(166).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(167).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(167).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(168).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(168).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(169).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(169).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(170).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(170).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(171).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(171).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(172).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(172).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(173).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(173).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(174).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(174).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(175).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(175).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(176).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(176).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(177).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(177).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(178).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(178).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(179).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(179).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(180).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(180).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(181).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(181).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(182).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(182).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(183).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(183).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(184).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(184).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(185).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(185).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(186).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(186).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(187).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(187).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(188).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(188).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(189).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(189).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(190).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(190).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(191).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(191).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(192).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(192).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(193).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(193).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(194).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(194).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(195).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(195).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(196).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(196).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(197).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(197).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(198).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(198).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(199).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(199).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(200).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(200).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(201).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(201).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(202).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(202).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(203).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(203).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(204).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(204).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(205).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(205).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(206).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(206).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(207).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(207).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(208).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(208).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(209).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(209).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(210).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(210).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(211).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(211).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(212).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(212).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(213).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(213).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(214).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(214).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(215).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(215).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(216).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(216).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(217).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(217).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(218).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(218).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(219).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(219).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(220).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(220).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(221).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(221).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>
    <div class="col d-flex justify-content-center p-0">
      <div class="card" style="width: 28rem;">
      <img class="lazy card-img-top" style="height:20rem;" data-src="/imgs/events/BLUE2020/blues-2020-(222).jpg" alt="La Puerta Del Sol, Playas del Coco, Costa Rica">
        <div class="card-body mx-auto">
          <a  download="bluesfest-2020.jpg" href="/imgs/events/BLUE2020/blues-2020-(222).jpg">
            <button type="button" class="btn btn-primary my-2">
              Descargar
            </button>
          </a>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
