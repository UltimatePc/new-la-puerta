@extends('layouts.guest')

@section('title', 'Gallery')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Gallery')
@section('langSwitch', '/galeria')

@include('components.navbar', ['active' => 'Gallery'])

@section('content')


<div class="container-fluid pt-lg-1 text-center">
  <div class="row">
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/BLUE2020/blues-2020-(202).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Blues Festival event 2020</h5>
        <p class="card-text">Hotel La Puerta del Sol would like to thank everyone who was able to join us on this excellent day!</p>
        <a href="{{ route('bluesfest2020') }}">
          <button type="button" class="btn btn-primary">Picture Gallery</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/CARNIVAL2020/carnival-2020-(228).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Carnival event 2020</h5>
        <p class="card-text">Hotel La Puerta del Sol would like to thank everyone who was able to join us on this excellent day!</p>
        <a href="{{ route('carnival2020') }}">
          <button type="button" class="btn btn-primary">Picture Gallery</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/PAPA2019/taste-of-papagayo-(49).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Taste of Papagayo 2019</h5>
        <p class="card-text">Hotel La Puerta del Sol would like to thank everyone who made Taste of Papagayo possible this year.
          Restaurants, our staff and the community, we love and appreciate you all, it would not be possible without you :) Thank you for all your support!</p>
        <a href="{{ route('papagayo2019') }}">
          <button type="button" class="btn btn-primary">Picture Gallery</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/OCEAN2019/ocean-fest-2019-(120).JPG" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Ocean Fest 2019</h5>
        <p class="card-text">Ocean Fest! June 8th at Hotel La Puerta del Sol. Sponsored by Rich Coast Diving.</p>
        <a href="{{ route('oceanfest2019') }}">
          <button type="button" class="btn btn-primary">Picture Gallery</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/PDS2019/leatherback-pds-(104).JPG" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">PDS Leatherbacks 2019</h5>
        <p class="card-text">Donnie Walsh and The Leatherbacks :) Thank you Kimberly Irons for these amazing photos. What a great night at La Puerta del Sol.</p>
        <a href="{{ route('leatherback2019') }}">
          <button type="button" class="btn btn-primary">Picture Gallery</button>
        </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/events/PAPA2018/taste-of-papagayo-(21).jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Taste of Papagayo 2018</h5>
        <p class="card-text">Hotel La Puerta del Sol would like to thank everyone who made Taste of Papagayo possible this year.
          Restaurants, our staff and the community, we love and appreciate you all, it would not be possible without you :) Thank you for all your support!</p>
          <a href="{{ route('papagayo2018') }}">
            <button type="button" class="btn btn-primary">Picture Gallery</button>
          </a>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/Neotics-made-me-funky.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Neotics - Made me Funky (Funk Fest 2019)</h5>
        <p class="card-text">Live performance at the puerat del sol with Neotics playing "Made me Funky"</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#neoticsFunky">Video</button>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/Soul-kitchen-whats-going-on.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Soul Kitchen - What's Going On (Funk Fest 2019)</h5>
        <p class="card-text">Live performance at the puerta del sol with Soul Kitchen playing "What's going on?"</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#soulGoingOn">Video</button>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/Soul-kitchen-respect.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Soul Kitchen - Respect (Funk Fest 2019)</h5>
        <p class="card-text">Live performance at the puerta del sol with Soul Kitchen playing "Respect"</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#soulRespect">Video</button>
      </div>
    </div>
    <div class="card col-12 col-lg-4 p-0">
      <img data-src="/imgs/vids/event-easter.jpg" class="lazy card-img-top" alt="Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-body">
        <h5 class="card-title">Easter Event 2019</h5>
        <p class="card-text">Thank you for being with us on such a special day for children!</p>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#easterEvent2019">Video</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('modals')

<div class="modal fade" id="neoticsFunky" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Neotics - Made me funky</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/L8ox2ZRnnYo?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="soulGoingOn" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Soul Kitchen - Whats going on?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/TLEtsShghj4?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="soulRespect" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Soul Kitchen - Respect</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/9jbu7dULL_8?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="easterEvent2019" tabindex="-1" role="dialog" aria-labelledby="neoticsFunkyLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Easter Event - 2019</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe class="lazy" data-src="https://www.youtube.com/embed/IV6lKCnvXa8?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
