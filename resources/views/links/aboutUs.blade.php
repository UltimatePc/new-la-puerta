@extends('layouts.guest')

@section('title', 'About Us')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'About Us')
@section('langSwitch', '/nosotros')


@section('content')

@include('components.navbar', ['active' => 'About Us'])

<div class="container-fluid">

  <div class="row my-lg-5 d-flex justify-content-center align-items-center border-bottom border-top">
    <h1 class="my-3 text-center">About La Puerta Del Sol</h1>
  </div>

  <div class="row">
    <div class="col-12 d-flex align-items-center px-0">
      <div class="container">
        <h1>Mission Statement:</h1>
        <p>
          Our mission is to provide our guests with personalized world-class hospitality to ensure absolute escapism in <b>La Puerta del Sol</b>.
        </p>
        <h2>The Approach:</h2>
        <p>
          La Puerta del Sol strives to provide outstanding lodging facilities and services to our guests.
          Our hotel focuses on creating a peaceful atmosphere with our garden and pool area and we emphasize that in how
          La Puerta del Sol is not only a hotel but a sanctuary to reach absolute relaxation.
        </p>
        <p>
          Our goal is to inspire travelers to draw closer to their surroundings, venture beyond the ordinary, and to see the world as spellbinding.
          What brings people back to the La Puerta del Sol is its peaceful grounds. We focus on keeping our garden in pristine condition on a daily basis.
        </p>
        <p>
          We encourage our guests to really draw closer to their surroundings when they stay with us.
          Our mission as a team at La Puerta del sol is to provide our guests with personalized world-class hospitality to ensure absolute escapism in our hotel.
        </p>
      </div>
    </div>

  </div>
</div>

@endsection
