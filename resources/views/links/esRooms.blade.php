@extends('layouts.guest')

@section('title', 'Habitaciones')
@section('id', 'La Puerta Del Sol')
@section('pageName', 'Habitaciones')
@section('langSwitch', '/rooms')

@section('content')

@include('components.esNavbar', ['active' => 'Habitaciones'])

<div class="container-fluid pt-lg-1 text-center">

  <!-- K007 Suites -->
  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0 mb-5 mb-lg-0 d-flex justify-content-end">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        K-007 Suite
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#k007SuiteModal">
          Más información
        </button>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#k007SuiteModalV">
          Video del Cuarto
        </button>
      </h1>
    </div>
    <div class="col-12 col-lg-6 px-0 order-first order-lg-last">
      @include('components/carousel.roomK007')
    </div>
  </div>

  <!-- Master Suites -->
  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0">
      @include('components/carousel.roomMaster')
    </div>
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-start mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        Master Suites
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#masterSuiteModal">
          Más información
        </button>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#masterSuiteModalV">
          Video del Cuarto
        </button>
      </h1>
    </div>
  </div>

  <!-- Junior Suites -->
  <div class="row d-flex align-items-center mb-lg-0">
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-end mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
        Junior Suites
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#juniorSuiteModal">
          Más información
        </button>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#juniorSuiteModalV">
          Video del Cuarto
        </button>
      </h1>
    </div>
    <div class="col-12 col-lg-6 px-0 order-first order-lg-last">
      @include('components/carousel.roomJunior')
    </div>
  </div>

  <!-- Standard Suites -->
  <div class="row d-none justify-content-center align-items-center">
    <div class="col-12 col-lg-6 px-0">
      @include('components/carousel.roomStandard')
    </div>
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-start mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
          Suites Estándar
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#standardSuiteModal">
          Más información
        </button>
      </h1>
    </div>
  </div>

  <!-- Economy Suites -->
  <div class="row d-none align-items-center mb-lg-0">
    <div class="col-12 col-lg-6 px-0 my-lg-5 d-flex justify-content-end mb-5 mb-lg-0">
      <h1 class="my-lg-5 py-lg-5 text-box-room px-5">
          Suites Económicas
        <br>
        <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#economySuiteModal">
          Más información
        </button>
      </h1>
    </div>
    <div class="col-12 col-lg-6 px-0 order-first order-lg-last">
      @include('components/carousel.roomEconomy')
    </div>
  </div>

</div>

@endsection

@section('modals')

<!-- Modals for Economy Suites -->
<div class="modal fade" id="economySuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableEconomy" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableEconomy">Economy Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomEconomyDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
            <h3>Tarifas :</h3>
              <p>Temporada actual: <b>(Temporada Alta Tarifas)</b></p>
              <p>1 de Diciembre - 30 de Abril</p>
              <br>
              <p><b>Single - $70.00 + Tax</b></p>
              <p><b>Double - $85.00 + Tax</b></p>
              <p><b>Kids from 6-11 - $8.00</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomEconomyDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Alta Tarifas</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Tarifas :</h3>
              <p>Siguiente Temporada : <b>(Tarifas de temporada verde)</b></p>
              <p>1 de Mayo - 30 de Noviembre</p>
              <br>
              <p><b>Single - $55.00 + Tax</b></p>
              <p><b>Double - $70.00 + Tax</b></p>
              <p><b>Kids from 6-11 - $8.00</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomEconomyDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Verde Tarifas</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Desayuno incluido </p>
        <p>- Las tarifas están en dólares estadounidenses </p>
        <p>- Los niños de 0 a 5 años se hospedan gratis, se aplica a 1 niño por habitación </p>
        <p>- Los niños de 6 a 11 años pagan $8.00 + impuestos </p>
        <p>- Prepago </p>
        <b><i>
          A poca distancia de la playa y a 100 metros del check in y del principio La Puerta de Sol
        </i></b>
        <p class="mt-3">
          Cada habitación tiene un desayuno de cortesía que se sirve en el hotel principal y tiene nuevas unidades de aire acondicionado, televisores inteligentes de 43 pulgadas, refrigeradores, cafeteras, WIFI gratuito, agua caliente y seguridad las 24 horas, los 7 días de la semana
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Standard Suites -->
<div class="modal fade" id="standardSuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableStandard" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableStandard">Standard Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomStandardDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
            <h3>Tarifas :</h3>
              <p>Temporada actual: <b>(Temporada Alta Tarifas)</b></p>
              <p>1 de Diciembre - 30 de Abril</p>
              <br>
              <p><b>Doble - $140.00 + Impuestos</b></p>
              <p><b>Persona Extra - $45.00 + Impuestos</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomStandardDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Alta Tarifas</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Tarifas :</h3>
              <p>Siguiente Temporada : <b>(Tarifas de temporada verde)</b></p>
              <p>1 de Mayo - 30 de Noviembre</p>
              <br>
              <p><b>Doble - $90.00 + Impuestos</b></p>
              <p><b>Tercera persona - $45.00 + Impuestos</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomStandardDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Verde Tarifas</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Desayuno incluido </p>
        <p>- Las tarifas están en dólares estadounidenses </p>
        <p>- Los precios incluyen 2 personas, $15.00 + impuestos por persona después de las primeras 2 </p>
        <p>- Los niños de 0 a 5 años se hospedan gratis, se aplica a 1 niño por habitación </p>
        <p>- Los niños de 6 a 11 años pagan $8.00 + impuestos </p>
        <p>- Prepago </p>
        <b><i>
          A poca distancia de la playa y a 100 metros del check in y del principio La Puerta de Sol
        </i></b>
        <p class="mt-3">
        Cada habitación tiene un desayuno de cortesía que se sirve en el hotel principal y tiene nuevas unidades de aire acondicionado, televisores inteligentes de 43 pulgadas, refrigeradores, cafeteras, WIFI gratuito, agua caliente y seguridad las 24 horas, los 7 días de la semana. Estas habitaciones cuentan con la opción de camas dobles y amplios armarios
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Junior Suites -->
<div class="modal fade" id="juniorSuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableJunior" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableJunior">Junior Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomJuniorDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
              <h3>Tarifas :</h3>
              <p>Temporada actual: <b>(Temporada Alta Tarifas)</b></p>
              <p>1 de Diciembre - 30 de Abril</p>
              <br>
              <p><b>Solo - $100.00 + Impuestos</b></p>
              <p><b>Doble - $140.00 + Impuestos</b></p>
              <p><b>Persona Extra - $45.00 + Impuestos</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomJuniorDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Alta Tarifas</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Tarifas :</h3>
              <p>Siguiente Temporada : <b>(Tarifas de temporada verde)</b></p>
              <p>1 de Mayo - 30 de Noviembre</p>
              <br>
              <p><b>Solo - $80.00 + Impuestos</b></p>
              <p><b>Doble - $100.00 + Impuestos</b></p>
              <p><b>Tercera persona - $45.00 + Impuestos</b></p>
              <p><b>(Max 3 Pax)</b></p>
              <a href="#carouselRoomJuniorDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Verde Tarifas</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Desayuno incluido </p>
        <p>- Las tarifas están en dólares estadounidenses </p>
        <p>- Los precios incluyen 2 personas, $ 45.00 + impuestos por persona después de las primeras 2 </p>
        <p>- Los niños de 0 a 5 años se hospedan gratis, se aplica a 1 niño por habitación </p>
        <p>- Los niños de 6 a 11 años pagan $ 22.00 + impuestos </p>
        <p>- Prepago </p>
        <p class="my-3">
          *Las comodidades incluyen :
          <i class="fas fa-tv"></i> Cable y TV , <i class="far fa-snowflake"></i> Aire acondicionado Nuevo, <i class="fas fa-wifi"></i> Wifi ,
          <i class="fab fa-hotjar"></i> Agua caliente , <i class="fas fa-archive"></i> Mini Nevera , <i class="fas fa-car"></i> Estacionamiento , <i class="fas fa-star"></i> Seguro,
          <i class="fas fa-star"></i> Microondas, <i class="fas fa-star"></i> Coffee-Maker/Cafetera , <i class="fas fa-paw"></i> ¡Mascota amigable! (requiere tarifas adicionales)
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect?locale=es&check_in_date=11-01-2021&check_out_date=12-01-2021&number_adults=2">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Video Junior Suites -->
<div class="modal fade" id="juniorSuiteModalV" tabindex="-1" role="dialog" aria-labelledby="modalLableJuniorV" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableJuniorV">Video de Junior Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <iframe class="lazy" data-src="https://www.youtube.com/embed/hd8mEkFRa3c?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect?locale=es&check_in_date=11-01-2021&check_out_date=12-01-2021&number_adults=2">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Master Suites -->
<div class="modal fade" id="masterSuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableMaster" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableMaster">Master Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomMasterDetails" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center">
              <h3>Tarifas :</h3>
              <p>Temporada actual: <b>(Temporada Alta Tarifas)</b></p>
              <p>1 de Diciembre - 30 de Abril</p>
              <br>
              <p><b>Solo - $130.00 + Impuestos</b></p>
              <p><b>Doble - $160.00 + Impuestos</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomMasterDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Verde Tarifas</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Tarifas :</h3>
              <p>Siguiente Temporada : <b>(Tarifas de temporada verde)</b></p>
              <p>1 de Mayo - 30 de Noviembre</p>
              <br>
              <p><b>Solo - $110.00 + Impuestos</b></p>
              <p><b>Doble - $140.00 + Impuestos</b></p>
              <p><b>(Max 2 Pax)</b></p>
              <a href="#carouselRoomMasterDetails" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Alta Tarifas</button>
              </a>
            </div>
          </div>
        </div>
        <p>- Desayuno incluido </p>
        <p>- Las tarifas están en dólares estadounidenses </p>
        <p>- Los precios incluyen 2 personas, $ 45.00 + impuestos por persona después de las primeras 2 </p>
        <p>- Los niños de 0 a 5 años se hospedan gratis, se aplica a 1 niño por habitación </p>
        <p>- Los niños de 6 a 11 años pagan $ 22.00 + impuestos </p>
        <p>- Prepago </p>
        <p class="my-3">
          *Las comodidades incluyen :
          <i class="fas fa-tv"></i> Cable y TV , <i class="far fa-snowflake"></i> Aire acondicionado Nuevo, <i class="fas fa-wifi"></i> Wifi ,
          <i class="fas fa-archive"></i> Mini Nevera , <i class="fab fa-hotjar"></i> Agua caliente ,
          <i class="fas fa-car"></i> Estacionamiento , <i class="fas fa-star"></i> Seguro,
          <i class="fas fa-star"></i> Microondas, <i class="fas fa-star"></i> Coffee-Maker/Cafetera , <i class="fas fa-paw"></i> ¡Mascota amigable! (requiere tarifas adicionales)
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect?locale=es&check_in_date=11-01-2021&check_out_date=12-01-2021&number_adults=2">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Video Master Suites -->
<div class="modal fade" id="masterSuiteModalV" tabindex="-1" role="dialog" aria-labelledby="modalLableMasterV" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableMasterV">Video de Master Suites</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <div class="container">
            <iframe class="lazy" data-src="https://www.youtube.com/embed/PEAMkmSuTro?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect?locale=es&check_in_date=11-01-2021&check_out_date=12-01-2021&number_adults=2">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for K-007 Suite -->
<div class="modal fade" id="k007SuiteModal" tabindex="-1" role="dialog" aria-labelledby="modalLableK007" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableK007">K-007 Suite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="carouselRoomK007Details" class="carousel slide carousel-fade" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active text-center px-lg-5">
              <h3>Tarifas :</h3>
              <p>Temporada actual: <b>(Temporada Alta Tarifas)</b></p>
              <p>1 de Diciembre - 30 de Abril</p>
              <br>
              <p><b>Solo - $180.00 + Impuestos</b></p>
              <p><b>Doble - $200.00 + Impuestos</b></p>
              <p><b>Persona Extra - $45.00 + Impuestos</b></p>
              <p><b>(Max 6 Pax)</b></p>
              <a href="#carouselRoomK007Details" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Verde Tarifas</button>
              </a>
            </div>
            <div class="carousel-item text-center">
              <h3>Tarifas :</h3>
              <p>Siguiente Temporada : <b>(Tarifas de temporada verde)</b></p>
              <p>1 de Mayo - 30 de Noviembre</p>
              <br>
              <p><b>Doble - $120.00 + Impuestos</b></p>
              <p><b>Doble - $160.00 + Impuestos</b></p>
              <p><b>Tercera persona - $45.00 + Impuestos</b></p>
              <p><b>(Max 6 Pax)</b></p>
              <a href="#carouselRoomK007Details" role="button" data-slide="next">
                <button type="button" class="btn btn-primary my-5">Temporada Alta Tarifas</button>
              </a>
            </div>
          </div>
        </div>
        <p>
          <i>
            Disfrute de un estilo de vida de apartamentos de calidad dentro del jardín de la puerta del sol, esta es la suite de lujo con 2 dormitorios,
            una cama principal y 2 camas adicionales, una cocina eficiente para todas sus necesidades, muebles nuevos hechos a medida, televisión inteligente y más.
          </i>
        </p>
        <p>- Desayuno incluido </p>
        <p>- Las tarifas están en dólares estadounidenses </p>
        <p>- Los precios incluyen 2 personas, $ 45.00 + impuestos por persona después de las primeras 2 </p>
        <p>- Los niños de 0 a 5 años se hospedan gratis, se aplica a 1 niño por habitación </p>
        <p>- Los niños de 6 a 11 años pagan $ 22.00 + impuestos </p>
        <p>- Prepago </p>
        <p class="my-3">
          *Las comodidades incluyen :
          <i class="fas fa-tv"></i> Cable y TV , <i class="far fa-snowflake"></i> Aire acondicionado Nuevo, <i class="fas fa-wifi"></i> Wifi ,
          <i class="fas fa-archive"></i> Mini Nevera , <i class="fas fa-utensils"></i> Cocina Efficient, <i class="fab fa-hotjar"></i> Agua caliente ,
          <i class="fas fa-car"></i> Estacionamiento , <i class="fas fa-star"></i> Seguro, <i class="fas fa-paw"></i> ¡Mascota amigable! (requiere tarifas adicionales)
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect?locale=es&check_in_date=11-01-2021&check_out_date=12-01-2021&number_adults=2">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Modals for Video K-007 Suite -->
<div class="modal fade" id="k007SuiteModalV" tabindex="-1" role="dialog" aria-labelledby="modalLableK007V" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLableK007V">Video del K-007 Suite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <div class="container">
            <iframe class="lazy" data-src="https://www.youtube.com/embed/sXbXilpBFTc?rel=0&amp;autoplay=0&showinfo=0" width="100%" height="400px" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <a href="https://app.thebookingbutton.com/properties/hotellapuertadelsoldirect?locale=es&check_in_date=11-01-2021&check_out_date=12-01-2021&number_adults=2">
          <button type="button" class="btn btn-primary">Reservar Ahora</button>
        </a>
      </div>
    </div>
  </div>
</div>
