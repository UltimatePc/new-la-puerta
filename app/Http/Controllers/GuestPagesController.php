<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
         return view('links.index');
     }

     public function esIndex()
     {
         return view('links.esIndex');
     }

     public function rooms()
     {
         return view('links.rooms');
     }

     public function esRooms()
     {
         return view('links.esRooms');
     }

     public function news()
     {
         return view('links.news');
     }

     public function esNews()
     {
         return view('links.esNews');
     }

     public function suggestion()
     {
         return view('links.suggestion');
     }

     public function esSuggestion()
     {
         return view('links.esSuggestion');
     }

     public function gallery()
     {
         return view('links.gallery');
     }

     public function esGallery()
     {
         return view('links.esGallery');
     }

     public function contactUs()
     {
         return view('links.contactUs');
     }

     public function esContactUs()
     {
         return view('links.esContactUs');
     }

     public function aboutUs()
     {
         return view('links.aboutUs');
     }

     public function esAboutUs()
     {
         return view('links.esAboutUs');
     }

     public function activities()
     {
         return view('links.activities');
     }

     public function esActivities()
     {
         return view('links.esActivities');
     }

     public function papagayo2019()
     {
         return view('links/gallery.papagayo2019');
     }

     public function oceanfest2019()
     {
         return view('links/gallery.oceanfest2019');
     }

     public function leatherback2019()
     {
         return view('links/gallery.leatherback2019');
     }

     public function papagayo2018()
     {
         return view('links/gallery.papagayo2018');
     }

     public function carnival2020()
     {
         return view('links/gallery.carnival2020');
     }

     public function bluesfest2020()
     {
         return view('links/gallery.bluesfest2020');
     }

     public function espapagayo2019()
     {
         return view('links/gallery.espapagayo2019');
     }

     public function esoceanfest2019()
     {
         return view('links/gallery.esoceanfest2019');
     }

     public function esleatherback2019()
     {
         return view('links/gallery.esleatherback2019');
     }

     public function espapagayo2018()
     {
         return view('links/gallery.espapagayo2018');
     }

     public function escarnival2020()
     {
         return view('links/gallery.escarnival2020');
     }

     public function esbluesfest2020()
     {
         return view('links/gallery.esbluesfest2020');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
