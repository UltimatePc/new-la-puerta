<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    public function create()
    {
        return view('links.suggestion');
    }

    public function esCreate()
    {
        return view('links.esSuggestion');
    }

    public function store (Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'message' => 'required'
      ]);

      Mail::send('emails.suggestion-message', [
        'name' => $request->name,
        'email' => $request->email,
        'msg' => $request->message
      ], function ($mail) use($request) {
        $mail->from('postmaster@lapuertadelsolhotel.com', $request->name);

        $mail->to('andre.rojas.esquivel@gmail.com')->subject('La puerta Suggestion');
      });

      return redirect()->back()->with('flash_message', 'Thank you for reaching out to us!');
    }
}
