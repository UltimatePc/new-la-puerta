<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestPagesController@index')->name('index');
Route::get('/news', 'GuestPagesController@news')->name('news');
Route::get('/rooms', 'GuestPagesController@rooms')->name('rooms');
Route::get('/gallery', 'GuestPagesController@gallery')->name('gallery');
Route::get('/aboutUs', 'GuestPagesController@aboutUs')->name('aboutUs');
Route::get('/contactUs', 'GuestPagesController@contactUs')->name('contactUs');
Route::get('/suggestion', 'ContactFormController@create')->name('suggestion');
Route::get('/activities', 'GuestPagesController@activities')->name('activities');
Route::get('/gallery/carnival2020', 'GuestPagesController@carnival2020')->name('carnival2020');
Route::get('/gallery/papagayo2018', 'GuestPagesController@papagayo2018')->name('papagayo2018');
Route::get('/gallery/papagayo2019', 'GuestPagesController@papagayo2019')->name('papagayo2019');
Route::get('/gallery/bluesfest2020', 'GuestPagesController@bluesfest2020')->name('bluesfest2020');
Route::get('/gallery/oceanfest2019', 'GuestPagesController@oceanfest2019')->name('oceanfest2019');
Route::get('/gallery/leatherback2019', 'GuestPagesController@leatherback2019')->name('leatherback2019');

Route::get('/es', 'GuestPagesController@esIndex')->name('esIndex');
Route::get('/noticias', 'GuestPagesController@esNews')->name('esNews');
Route::get('/habitaciones', 'GuestPagesController@esRooms')->name('esRooms');
Route::get('/galeria', 'GuestPagesController@esGallery')->name('esGallery');
Route::get('/nosotros', 'GuestPagesController@esAboutUs')->name('esAboutUs');
Route::get('/sugerencia', 'ContactFormController@esCreate')->name('esSuggestion');
Route::get('/contactenos', 'GuestPagesController@esContactUs')->name('esContactUs');
Route::get('/actividades', 'GuestPagesController@esActivities')->name('esActivities');
Route::get('/gallery/escarnival2020', 'GuestPagesController@escarnival2020')->name('escarnival2020');
Route::get('/gallery/espapagayo2018', 'GuestPagesController@espapagayo2018')->name('espapagayo2018');
Route::get('/gallery/espapagayo2019', 'GuestPagesController@espapagayo2019')->name('espapagayo2019');
Route::get('/gallery/esbluesfest2020', 'GuestPagesController@esbluesfest2020')->name('esbluesfest2020');
Route::get('/gallery/esoceanfest2019', 'GuestPagesController@esoceanfest2019')->name('esoceanfest2019');
Route::get('/gallery/esleatherback2019', 'GuestPagesController@esleatherback2019')->name('esleatherback2019');

Route::post('/suggestion', [
  'uses' => 'ContactFormController@store',
  'as' => 'suggestion.store'
]);
